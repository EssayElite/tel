#include "Solver.h"
#include <cstdlib>

namespace NSPCA {
    void Solver::transformed_score(unsigned int numThreadsGPU, unsigned int numThreadsGPUZ) {
        cumulate_gpu(numThreadsGPU);

        int numThreads = pools.threads.getNumThreads() + 1;
        int step_size = dim.no_var / numThreads;

        int start = 0;
        int end = step_size;
        vector<std::future<void>> futures((unsigned long) numThreads - 1);
        for (int thread = 0; thread < numThreads - 1; thread++) {
            auto task = internal::solve_transformed_score_cols(count.incidence_count, count.incidence_count_score,
                                                               count.transformed_score_solution, constants.scale,
                                                               constants.square_n, start, end, this->dim);
            futures[thread] = pools.threads.submit(task);
            start = end ;
            end += step_size;
        }

        auto task = internal::solve_transformed_score_cols(count.incidence_count, count.incidence_count_score,
                                                           count.transformed_score_solution, constants.scale,
                                                           constants.square_n, start, dim.no_var, this->dim);
        task();
//        auto tmp = 0;
        for (int thread = 0; thread < numThreads - 1; thread++) {
            futures[thread].get();
        }

        set_z(numThreadsGPUZ);

//        std::cout << "Transformed score from CPU is " << std::endl << solutionG.transformed_score << std::endl;
//        int start =0; int end = dim.no_var;
//        auto task = internal::solve_transformed_score_cols(count.incidence_count, count.incidence_count_score,
//                                                           count.transformed_score_solution, constants.scale,
//                                                           constants.square_n, start, dim.no_var, this->dim);
//        task();
//
//        set_z(numThreadsGPU);
    }

    void Solver::cumulate_gpu(unsigned int numThreads) {
        solver.matmul_nn(1.0, solutionG.principal_score, solutionG.component_loading, 0.0, tmp_matrices.AP);
//        std::cout << "Matrix AP is " << std::endl;
//        std::cout << tmp_matrices.AP << std::endl;
//        std::cout << "Data is" << std::endl << data_gpu << std::endl;
        solver.sync();
//        std::cout << tmp_matrices.AP << std::endl;
        cumulate_kernel_gpu(data_gpu.data(), dim.no_obs, dim.no_var, tmp_matrices.AP.data(),
                            count.incidence_count_score_gpu.data(),
                            numThreads, pools.streams.getCurrentStream());
        pools.streams.syncAll();
        count.copy_score_to_cpu(pools.streams.getCurrentStream());
        pools.streams.syncAll();

    }

    void Solver::principal_score() {
        solver.matmul_nt(1.0, solutionG.transformed_score, solutionG.component_loading, 0.0, tmp_matrices.ZPT);
        solver.sync();
//        std::cout << "In C++, ZPT is " << std::endl << tmp_matrices.ZPT << std::endl;
        solver.thin_svd(tmp_matrices.ZPT, tmp_matrices.U, tmp_matrices.V, tmp_matrices.diag);
        solver.sync();
//        std::cout << "In c++, U is " << std::endl << tmp_matrices.UView << std::endl;
//        std::cout << "In c++, V^T is " << std::endl << tmp_matrices.V <<std::endl;
//        cuStat::inverseWithEigenTranspose(tmp_matrices.V, pools.streams.getCurrentStream());
//        pools.streams.syncAll();

//        std::cout << "In C++, V inverse is " << std::endl << tmp_matrices.V << std::endl;
//        std::cout << constants.scale << std::endl;
//        std::cout << constants.square_n << std::endl;
        solver.matmul_nt(constants.scale * constants.square_n, tmp_matrices.UView, tmp_matrices.V, 0.0,
                         solutionG.principal_score);
        solver.sync();
//        std::cout << "From C++ " << std::endl << solutionG.principal_score << std::endl;
    }

    void Solver::component_loading(int numThreads, int numBlocks) {
        solver.matmul_tn(1.0, solutionG.principal_score, solutionG.transformed_score, 0.0, tmp_matrices.ATZ);
//        std::cout << "ATZ = " << std::endl <<tmp_matrices.ATZ << std::endl;
        solver.sync();

        solve_p_nspca(solutionG.component_loading.data(), dim.no_obs, dim.no_var, dim.reduce_dim,
                      tmp_matrices.ATZ.data(), restriction_gpu.data(), constants.n_alpha, constants.n_scale_square,
                      numThreads,
                      numBlocks, pools.streams.getCurrentStream());

    }

    void Solver::get_principal_score(double *dst) {
        solutionG.get_principal_score(dst, pools.streams.getCurrentStream());
        pools.streams.syncAll();
    }

    void Solver::get_transformed_score(double *dst) {
        solutionG.get_transformed_score(dst, pools.streams.getCurrentStream());
        pools.streams.syncAll();
    }

    void Solver::get_component_loading(double *dst) {
        solutionG.get_component_loading(dst, pools.streams.getCurrentStream());
        pools.streams.syncAll();
    }

    void Solver::set_alpha(const double new_alpha) {
        constants.alpha = new_alpha;
        constants.n_alpha = constants.no_obs * new_alpha;

    }

    void Solver::get_cumu_score(double *dst) {
        cuStat::copy_to_cpu(dst, count.incidence_count_score_gpu, pools.streams.getCurrentStream());
        pools.streams.syncAll();
    }

    void Solver::get_incidence_count(int *dst) {
        memcpy(dst, count.incidence_count, sizeof(int) * 3 * dim.no_var);
    }

    void Solver::set_z(unsigned int numThreadsGPUZ) {
        count.copy_solution_to_gpu(pools.streams.getCurrentStream());
        set_z_gpu(data_gpu.data(), solutionG.transformed_score.data(), count.transformed_score_solution_gpu.data(),
                  dim.no_obs, dim.no_var, numThreadsGPUZ, pools.streams.getCurrentStream());
        pools.streams.syncAll();
    }

    void
    Solver::solve(double *transformed_score, double *principal_score, double *component_loading, double lambda, double threshold,
                      int max_iter, unsigned int ts_thread1, unsigned int ts_thread2, unsigned int cl_thread, unsigned int cl_block) {
        int iter = 0;
        bool convergence = false;

        constants.set_lambda(lambda);

        while (iter < max_iter && !convergence){
            this->transformed_score(ts_thread1, ts_thread2);
            this->principal_score();
            this->component_loading(cl_thread, cl_block);

            auto diff = solutionG.compare_difference(pools.streams.getCurrentStream());
            cudaStreamSynchronize(pools.streams.getCurrentStream());

            if (std::abs(diff) < threshold) convergence = true;
            else {
                iter++;
                solutionG.copy_component_loading();
            }

        }

        cuStat::copy_to_cpu(principal_score, solutionG.principal_score, pools.streams.getCurrentStream());
        cuStat::copy_to_cpu(transformed_score, solutionG.transformed_score, pools.streams.getNextStream());
        cuStat::copy_to_cpu(component_loading, solutionG.component_loading, pools.streams.getNextStream());
        pools.streams.syncAll();
    }



}