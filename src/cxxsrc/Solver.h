#pragma once

#include <cassert>
#include "../nspcaCudalib/nspca_cuda.h"
//#include <Eigen/Dense>
#include <cuda_runtime.h>
#include <vector>

using std::vector;

#include "../custat/cuStat.h"
//#include "../custat/cuStatHost.h"
#include "transformed_score.h"
#include "Threadpool/thread_pool.h"
#include "transformed_score.h"

#include "util.h"
#include <thread>
#include <future>
#include <cmath>
#include <cstdlib>

extern "C" void cumulate_cpu_(const int *data, int *count, int no_obs, int no_var);

namespace NSPCA {

    class Solver {
    public:
        cuStat::linSolver solver;
        internal::dim dim;
        internal::constants constants;
        internal::tmp_matrices tmp_matrices;
        internal::IncidenceCount count;
        internal::pools pools;

        SolutionGPU solutionG;
        Solution solution;

        MatrixXi data_gpu;
        MatrixXi restriction_gpu;

//        Solver(size_t no_obs, size_t no_var, size_t reduced_dim, double scale,
//               unsigned int numThreads, int numStreams)
//                : solver(), dim(no_obs, no_var, reduced_dim), constants(no_obs, no_var, reduced_dim),
//                  tmp_matrices(no_obs, no_var, reduced_dim), count(no_var),
//                  pools(numThreads, numStreams),
//                  solutionG(no_obs, no_var, reduced_dim),
//                  solution(nullptr, nullptr, nullptr), data_gpu(no_obs, no_var),
//                  restriction_gpu(reduced_dim, no_var) {}

        Solver(int no_obs, size_t no_var, size_t reduced_dim, double scale,
               unsigned int numThreads, int numStreams)
                : solver(), dim(no_obs, no_var, reduced_dim), constants(no_obs, no_var, reduced_dim),
                  tmp_matrices(no_obs, no_var, reduced_dim), count(no_var), pools(numThreads, numStreams),
                  solutionG(no_obs, no_var, reduced_dim),
                  solution(nullptr, nullptr, nullptr), data_gpu(no_obs, no_var),
                  restriction_gpu(reduced_dim, no_var) {}

        void init(const int *data, const int *restriction, double *transformed_score, double *principal_score,
                  double *component_loading) {
            cuStat::copy_to_gpu(data, data_gpu, pools.streams.getCurrentStream());
            cuStat::copy_to_gpu(restriction, restriction_gpu, pools.streams.getNextStream());
            cuStat::copy_to_gpu(transformed_score, solutionG.transformed_score, pools.streams.getNextStream());
            cuStat::copy_to_gpu(principal_score, solutionG.principal_score, pools.streams.getNextStream());
            cuStat::copy_to_gpu(component_loading, solutionG.component_loading, pools.streams.getNextStream());
            int temp_no_obs = (int) dim.no_obs;
            int temp_no_var = (int) dim.no_var;
            cumulate_cpu_(data, count.incidence_count, temp_no_obs, temp_no_var);
            pools.streams.syncAll();
        }

        void solve(double *transformed_score, double *principal_score, double *component_loading, double lambda,
                   double threshold,
                   int max_iter, unsigned int ts_thread1, unsigned int ts_thread2, unsigned int cl_thread,
                   unsigned int cl_block);

        void transformed_score(unsigned int numThreadsGPU, unsigned int numThreadsGPUZ);

        void principal_score();

        void component_loading(int numThreads, int numBlocks);

        void cumulate_gpu(unsigned int numThreads);

        void get_transformed_score(double *dst);

        void get_principal_score(double *dst);

        void get_component_loading(double *dst);

        void set_alpha(const double new_alpha);

        void get_incidence_count(int *dst);

        void get_cumu_score(double *dst);

        void set_z(unsigned int numThreadsGPUZ);
    };


}; ////End of NSPCA