#pragma once

#include <cuda_runtime.h>
#include <cstdlib>
#include "Threadpool/thread_pool.h"
#include "../custat/cuStat.h"

using cuStat::MatrixXd;
using cuStat::MatrixXi;
using cuStat::ViewXi;
using cuStat::ViewXd;

#include <cmath>

namespace NSPCA {
    namespace internal {
        class IncidenceCount {
        public:
            ViewXi incidence_count_cpu_view;
            ViewXd incidence_count_score_view;
            MatrixXd incidence_count_score_gpu;
            int *incidence_count;
            double *incidence_count_score;
            MatrixXd transformed_score_solution_gpu;
            double *transformed_score_solution;

            IncidenceCount(const size_t no_var) : incidence_count_cpu_view(nullptr, 3, no_var),
                                                  incidence_count_score_view(
                                                          nullptr, 3, no_var), incidence_count_score_gpu(3, no_var),
                                                  transformed_score_solution_gpu(3, no_var) {
                incidence_count = (int *) malloc(sizeof(int) * 3 * no_var);
                incidence_count_score = (double *) malloc(sizeof(double) * 3 * no_var);
                incidence_count_cpu_view.ptrDevice = incidence_count;
                incidence_count_score_view.ptrDevice = incidence_count_score;
                transformed_score_solution = (double *) malloc(sizeof(double) * 3 * no_var);
            }

            ~IncidenceCount() {
                delete incidence_count;
                delete incidence_count_score;
                delete transformed_score_solution;
            }

            void copy_solution_to_gpu(cudaStream_t stream) {
                cudaMemcpyAsync(transformed_score_solution_gpu.data(), transformed_score_solution,
                                sizeof(double) * transformed_score_solution_gpu.rows() *
                                transformed_score_solution_gpu.cols(), cudaMemcpyHostToDevice, stream);
            }

            void copy_score_to_cpu(cudaStream_t stream) {
                cuStat::copy_to_cpu(incidence_count_score, incidence_count_score_gpu, stream);
//                std::cout << "From C++" << std::endl;
//                std::cout << incidence_count_score_gpu << std::endl;
            }

        };

        struct dim {
            size_t no_obs;
            size_t no_var;
            size_t reduce_dim;

            dim(size_t no_obs, size_t no_var, size_t reduce_dim) : no_obs(no_obs), no_var(no_var),
                                                                   reduce_dim(reduce_dim) {}
        };

        struct pools {
            ThreadPool threads;
            cuStat::streamPool streams;

            pools(const unsigned int numThreads, const int numStreams) : threads(numThreads), streams(numStreams) {}
        };

        struct tmp_matrices {
//            self.ZPT = np.zeros((no_obs, reduced_dim))
//            self.U = np.zeros((no_obs, reduced_dim))
//            self.V = np.zeros((reduced_dim, reduced_dim))
//            self.ATZ = np.zeros((reduced_dim, no_var))
//            self.AP = np.zeros((no_obs, no_var))
            MatrixXd ZPT;
            MatrixXd U;
            MatrixXd V;
            MatrixXd diag;
            MatrixXd ATZ;
            MatrixXd AP;
            ViewXd UView;

            tmp_matrices(size_t no_obs, size_t no_var, size_t reduced_dim) :
                    ZPT(no_obs, reduced_dim),
                    U(no_obs, no_var),
                    V(reduced_dim, reduced_dim),
                    diag(reduced_dim, 1),
                    ATZ(reduced_dim, no_var),
                    AP(no_obs, no_var), UView(U.data(), no_obs, reduced_dim) {}
        };

        struct constants {
//            self.scale = scale
//            self.scale_square = scale * scale
//            self.alpha = alpha
//            self.n_alpha = alpha * no_obs
//            self.square_n = sqrt(no_obs)
//            self.n_scale_square = scale * no_obs
            int no_obs;
            double scale;
            double scale_square;
            double alpha;
            double n_alpha;
            double square_n;
            double n_scale_square;

            constants(int no_obs, double alpha, double scale) {
                this->no_obs = no_obs;
                this->scale = scale;
                scale_square = scale * scale;
                this->alpha = alpha;
                n_alpha = no_obs * alpha;
                square_n = sqrt(double(no_obs));
                n_scale_square = no_obs * scale_square;
            }

            void set_lambda(double alpha_new){
                alpha=alpha_new;
                n_alpha = no_obs*alpha_new;
            }
        };
    }////End of namespace internal
    class Solution {
    public:
        double *get_transformed_score() const {
            return transformed_score;
        }

        double *get_principal_score() const {
            return principal_score;
        }

        double *get_component_loading() const {
            return component_loading;
        }

        double *transformed_score;
        double *principal_score;
        double *component_loading;


        Solution(double *_transformed_score, double *_principal_score, double *_component_loading) : transformed_score(
                _transformed_score), principal_score(_principal_score), component_loading(_component_loading) {}

    };

    class SolutionGPU {
    public:
        size_t reduce_dim;
        size_t no_var;
        MatrixXd transformed_score;
        MatrixXd principal_score;
        MatrixXd component_loading;
        double * component_loading_copy_new;
        double * component_loading_copy_old;

        SolutionGPU(size_t no_obs, size_t no_var, size_t reduce_dim) : reduce_dim(reduce_dim),
                                                                       no_var(no_var), transformed_score(no_obs, no_var),
                                                                       principal_score(no_obs, reduce_dim),
                                                                       component_loading(reduce_dim, no_var)
                                                                       {
                                                                           component_loading_copy_new = new double[reduce_dim*no_var];
                                                                           component_loading_copy_old = new double[reduce_dim*no_var];

                                                                           for (size_t i = 0;i < reduce_dim*no_var;i++){
                                                                               component_loading_copy_old[i]=100.0;
                                                                               component_loading_copy_new[i]=100.0;
                                                                           }
                                                                       }
        ~SolutionGPU(){
            delete component_loading_copy_old;
            delete component_loading_copy_new;
        }
        void get_transformed_score(double *dst, cudaStream_t stream);

        void get_principal_score(double *dst, cudaStream_t stream);

        void get_component_loading(double *dst, cudaStream_t stream);

        void copy_component_loading();

        double compare_difference(cudaStream_t stream);
    };

    double SolutionGPU::compare_difference(cudaStream_t stream) {
        cudaMemcpyAsync(component_loading_copy_new, component_loading.data(), sizeof(double)*reduce_dim*no_var, cudaMemcpyDeviceToHost, stream);
        cudaStreamSynchronize(stream);

        double diff = 0;

        for (auto i =0; i< reduce_dim*no_var;i++){
            diff += std::abs(component_loading_copy_new[i]-component_loading_copy_old[i]);
        }

        return diff;

    }
    void SolutionGPU::copy_component_loading() {
        memcpy(component_loading_copy_old, component_loading_copy_new, sizeof(double)*reduce_dim*no_var);
    }

    void SolutionGPU::get_component_loading(double *dst, cudaStream_t stream) {
        cuStat::copy_to_cpu(dst, component_loading, stream);
    }

    void SolutionGPU::get_principal_score(double *dst, cudaStream_t stream) {
        cuStat::copy_to_cpu(dst, principal_score, stream);
    }

    void SolutionGPU::get_transformed_score(double *dst, cudaStream_t stream) {
        cuStat::copy_to_cpu(dst, transformed_score, stream);
    }


}