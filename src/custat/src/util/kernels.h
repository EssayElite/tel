#pragma once

#include "cuda_runtime.h"
#include "macro"
//#include "../../../../../../../../usr/local/cuda-8.0/include/curand_mtgp32_kernel.h"
//#include "../../../../../../../../usr/local/cuda-8.0/include/device_launch_parameters.h"


namespace cuStat {
    namespace internal {
        template<typename LHS, typename RHS>
        CUDA_KERNEL void assign_matrix_kernel(LHS lhs, RHS rhs);

        //////////////////////////////////////////////////////////
        ////Implementations///////////////////////////////////////
        //////////////////////////////////////////////////////////
        template<typename LHS, typename RHS>
        CUDA_KERNEL void assign_matrix_kernel(LHS lhs, RHS rhs) {
            unsigned int tid = threadIdx.x;
            unsigned int bid = blockIdx.x;
            unsigned int numThreads = blockIdx.x;
            unsigned int numBlocks = gridDim.x;

            unsigned int stride = numThreads * numBlocks;
            unsigned int index = tid + bid * numThreads;
            unsigned int size = lhs.size();

            for (;index<size;index+=stride){
                lhs[index] = rhs[index];
            }

        };
    }////End of namespace internal
}////End of namespace cuStat