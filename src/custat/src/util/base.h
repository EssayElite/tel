#pragma once

#include "managed.h"
#include "exception.h"
#include "kernels.h"
//#include "../../../../../../../../usr/local/cuda-8.0/include/driver_types.h"
#include <iostream>
#include <cuda_runtime.h>

namespace cuStat {
    namespace internal {
        template<typename Scalar, typename Derived>
        class Base : public Managed {
        public:
        };

        template<typename Scalar, typename LHS, typename RHS>
        class AddOp : Base<Scalar, AddOp<Scalar, LHS, RHS>> {
        public:
        };

//        template<typename Scalar, template<typename ...> class DST>
//        struct add_one{};
//
//        template<typename Scalar, typename... Other, template<typename ...> class DST>
//        struct add_one<Scalar, DST<Scalar, Other...>>{
//            DST<Scalar, Other...> dst;
//
//            add_one(DST<Scalar, Other...> _dst):dst(_dst){}
//
//            CUDA_CALLABLE_MEMBER add_one(add_one<Scalar, DST<Scalar, Other...>> other):dst(other.dst){}
//        };

        template<typename RHS>
        class MatrixAssign {
        private:
            RHS rhs;
            unsigned int numThreads;
            unsigned int numBlocks;
            cudaStream_t stream;
        public:
            MatrixAssign(RHS _rhs, unsigned int _numThreads, unsigned int _numBlocks, cudaStream_t _stream) : rhs(_rhs),
                                                                                                              numThreads(
                                                                                                                      _numThreads),
                                                                                                              numBlocks(
                                                                                                                      _numBlocks),
                                                                                                              stream(_stream) {
                std::cout << "Has yet to implement check on the number of threads and blocks" << std::endl;
            }

            template<typename LHS>
            void run(LHS lhs) {
                assign_matrix_kernel << < numThreads, numBlocks, 0, stream >> > (lhs, rhs);
            }
        };


        template<typename DST>
        struct add_one {
            DST dst;
            using ScalarType = typename DST::ScalarType;
            CUDA_CALLABLE_MEMBER add_one(DST _dst) : dst(_dst) {}

            CUDA_CALLABLE_MEMBER add_one(const add_one &other) : dst(other.dst) {}

            CUDA_CALLABLE_MEMBER ScalarType operator()(unsigned int index) {
                return dst(index);
            }

            CUDA_CALLABLE_MEMBER ScalarType operator()(size_t index) {
                return dst(index);
            }

            CUDA_CALLABLE_MEMBER const MatrixAssign<add_one<DST>> &
            run(unsigned int numThreads, unsigned int numBlocks, cudaStream_t stream) {
                return MatrixAssign<add_one<DST>>(*this, numThreads, numBlocks, stream);
            }

        };


    }
}